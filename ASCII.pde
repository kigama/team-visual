//Basado en el post de Chris Webb
char[] chars ={'█','█','▓','▒','░','#','≡','%','$','@','&'};
String loadPath = "https://gamepedia.cursecdn.com/honkaiimpact3_gamepedia_en/thumb/2/29/Herrscher_of_the_Void.png/450px-Herrscher_of_the_Void.png?version=d34204275f24b44481321e0538f74515";
PImage picture;
int devisions = 3; // pixels
PFont font;
float xDiv;
float yDiv;

void setup(){
  picture = loadImage(loadPath);
  size(500,500);
  font = createFont("Georgia", 32);  
  xDiv = picture.width/devisions;
  yDiv = picture.height/devisions;
  println(xDiv);
}

void draw(){
  background(255);
  textFont(font,devisions+3);
  //Cambia entre la imagen original a la imagen procesada al dar clic
    if (mousePressed == true) {
    set(0, 0, picture);
  } else {
    for(int i = 0; i < height; i+= devisions){
    for(int j = 0; j < width; j+= devisions){
      color filler = picture.get(int(j),int(i));
      fill(filler);
      textAlign(CENTER);
      //gets char depending on brightness
      float value = brightness(filler);
      char charac = chars[int(value/25.5)];
      text(charac,(j),(i));
  }  
    }
  }
}

//Aumenta o disminuye en tamano de los caracteres
void keyPressed() {
  if (key == CODED) {
    if (keyCode == UP) {
      devisions++;
    } else if (keyCode == DOWN) {
      devisions--;
    } 
  }
  constrain(devisions,1,99);
}
