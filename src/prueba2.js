import React from "react";
import Sketch from "react-p5";


// import font from '.././assets/fonts/AppleBraille-Outline8Dot-100.vlw'
const P5GrayScale =()=> {
  
  const NormalizedBlurMask =
    [[1,1,1,1,1],
    [1,1,1,1,1],
    [1,1,1,1,1],
    [1,1,1,1,1],
    [1,1,1,1,1]]
  const GausianBlurMask =
    [[1,4,6,4,1],
    [4,16,24,16,4],
    [6,24,36,24,6],
    [4,16,24,16,4],
    [1,4,6,4,1]]
  const Mask = 
    [[-1, -1, -1, -1, 0],
    [-1, -1, -1, 0, 1],
    [-1, -1, 0, 1, 1],
    [-1, 0, 1, 1, 1],
    [0, 1, 1, 1, 1]]
  const EdgeMask=
    [[-1,-1,-1],
    [-1,8,-1],
    [-1,-1,-1]]
  var origen,transformed,initWidth,initHeight;
  var ultimaTecla = null
  var scale = 0.35
  var transformacion = 'Any'
  let Wa2000 = 'https://i.ibb.co/n7Rm8rJ/1008781.jpg'

  const  setup = (p5, canvasParentRef) => {
    initWidth = p5.windowWidth;
    initHeight = p5.windowHeight;
    p5.createCanvas(initWidth,initHeight).parent(canvasParentRef); // use parent to render canvas in this ref (without that p5 render this canvas outside your component)
    p5.textSize(32)
    origen = p5.loadImage(Wa2000)
    transformed = p5.loadImage(Wa2000)
    p5.textAlign(p5.CENTER, p5.TOP);
  };

  const draw = p5 => {
    p5.background('rgba(0,0,0, 0)');
    p5.text(`Transformada: ${transformacion}`,initWidth*0.05,10,initWidth*scale, 50);
    p5.fill(255,255,255)
    p5.stroke(0)
    p5.image(transformed,initWidth*0.05,50,initWidth*scale, initWidth*scale)
    p5.text(`Original al 80%`,initWidth/2.1,10,initWidth*scale*0.8,50);
    //al 80 por problemas con navegador
    p5.image(origen,initWidth/2.1,50,initWidth*scale*0.8, initWidth*scale*0.8)
  };

  const to_gray_scale = (r = 0.33,g=0.33,b=0.33) => {
    origen.loadPixels();
    transformed.loadPixels()
    for (var i=0; i < origen.width*origen.height*4;i+=4){

      var y = r*(origen.pixels[i])  +
          g*(origen.pixels[i+1])+
          b*(origen.pixels[i+2]);
      transformed.pixels[i] = (y);
      transformed.pixels[i+1] = (y);
      transformed.pixels[i+2] = (y);
    }
    transformed.updatePixels();
  }

  const reset_image =()=>{
    origen.loadPixels()
    transformed.loadPixels()
    for (var i=0; i < origen.width*origen.height*4;i+=4){
      transformed.pixels[i] = origen.pixels[i];
      transformed.pixels[i+1] = origen.pixels[i+1];
      transformed.pixels[i+2] = origen.pixels[i+2];
    }
    transformed.updatePixels()
  }

  const keyPressed = p5 =>{
    if (ultimaTecla ==='1'){
      if (p5.key==='a'){
        //Promedio rgb para cada prixelito
        to_gray_scale()
        setTransform(p5,'Escala grises')
      }else if (p5.key==='l'){
        // luma, se recorre cada pixel de la imagen
        to_gray_scale(0.299,0.587,0.114)
        setTransform(p5,'GS luma')
      }else if (p5.key === 'r'){
        reset_image()
        setTransform(p5,'Reseteo')
      }else if(p5.key === 'e'){
        convolution(EdgeMask)
        setTransform(p5,'Strong Edges')
      }else if(p5.key=== 'n'){
        //http://homepages.inf.ed.ac.uk/rbf/CVonline/LOCAL_COPIES/PIRODDI1/NormConv/node1.html#SECTION00010000000000000000
        convolution(NormalizedBlurMask)
        setTransform(p5,'Normalized Blur')
      }else if(p5.key === 'g'){
        convolution(GausianBlurMask)
        //BLUR Executes a Gaussian blur with the level parameter specifying the extent of the blurring. If no parameter is used, the blur is equivalent to Gaussian blur of radius 1. Larger values increase the blur
        setTransform(p5,'Gaussian Blur')
      }else if(p5.key === 'm'){
        //Masks part of an image from displaying by loading another image and using its alpha channel as an alpha channel for this image
        convolution(Mask)
        setTransform(p5,'Mask')
      }
    }
    ultimaTecla = p5.key

  }
  const setTransform=(p5,s)=>{
    transformacion = s
    p5.clear();

  }

  const convolution =(mask)=>{

    origen.loadPixels();
    transformed.loadPixels()
    let column,row;
    let radius = Math.floor(mask.length/2)

    for (let k = 0; k < origen.width*origen.height;k++){
      let index = k*4
      row = Math.floor(k/origen.width)
      column = k%origen.width
      let nR=0,nB=0,nG=0,accum=0;
      for (let i = 0; i< mask.length; i++){
        for (let j = 0; j< mask.length; j++){
          let indexX = (column - radius + j +origen.width)%origen.width
          let indexY = (row - radius + i+origen.height)%origen.height
          nR += origen.pixels[(indexY*origen.width+indexX)*4] * mask[i][j]
          nG += origen.pixels[(indexY*origen.width+indexX)*4+1] * mask[i][j]
          nB += origen.pixels[(indexY*origen.width+indexX)*4+2] * mask[i][j]
          accum += mask[i][j]
        }
      }
      if( accum > 1){
        nR = nR/accum
        nG = nG/accum
        nB = nB/accum
      }
      transformed.pixels[index]= Math.round(nR)
      transformed.pixels[index+1]= Math.round(nG)
      transformed.pixels[index+2]= Math.round(nB)
    }
    transformed.updatePixels()


  }

  return(<Sketch setup={setup} draw={draw} keyPressed={keyPressed}/>)

}
export default P5GrayScale;