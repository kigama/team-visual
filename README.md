## Contents

- [Getting Started](#getting-started)
- [Acerca del proyecto](#tutorial)
- [Acerca de nosotros](#build-deployment)

## Getting Started

1. Instalar dependencias

    ```sh
    $ git clone https://gitlab.com/kigama/team-visual
    ```

2. Instalar dependencias

    ```sh
    $ npm install
    ```

3. Inicia un servidor webpack. El comando por defecto corre `webpack-dev-server` en local, [`localhost:8080`](http://localhost:8080).

    ```sh
    $ npm start
    # O si usa yarn 
    $ yarn start
    # en un puerto especifico
    $ yarn start --port=3000
    ```
4. Para construir el proyecto de la pagina.
    ```sh
    $ npm run-script build
    # O si usa yarn 
    $ yarn run-script build
    ```
   Puede desplegar la pagina en servicios como Surge, Firebase, etc.

5. Para desplegar proyecto en GitLab Pages
Tenga en cuenta que en el repositorio de GitLab tenga la carpeta dist generada por el paso 4 (no se modifico el archivo '.gitlab-ci' para que lo haga de forma predeterminada :( )).

    ```sh
    $ git add -f dist
    ```
con este comando se forzo que la carpeta dist quede agregada al repositorio, lo siguiente seria realizar el clasico commit y luego el push

El archivo `.gitlab-ci` esta configurado para que cuando se realice un push a la rama master se despliegue el proyecto en GitLab Pages, en este caso: [`kigama.gitlab.io/team-visual`](https://kigama.gitlab.io/team-visual/) 

## Acerca del proyecto

El proyecto usa la base de spectacle, se creo de la siguiente manera:
```sh
$  spectacle-boilerplate \--mode mdx \--dir "team-visual"
```
permite escribir la pagina mediante Slides en md, soportado con el modulo `react-p5` para ejecutar processing en React JS

## Acerca de nosotros

Somos estudiantes de Ingeniería de Sistemas de la Universidad Nacional de Colombia

    Jose Perdomo Saenz                  - jdperdomos
    Kevin Alejandro Vanegas Morales     - kavanegasm

Agradecimientos a @xideaxi por la ayuda en la integracion de Spectacle Presentation con GitLab pages

